const express = require('express');
const bodyParser = require('body-parser');
const barangRoutes = require('./routes/barangRoutes');
const { specs, swaggerUi } = require('./swaggerconfig');
const cors = require("cors");
const app = express();
app.use(cors());


// Middleware
app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
// Routes
app.use('/barang', barangRoutes);

// Start Server
const PORT = process.env.PORT || 3100;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
